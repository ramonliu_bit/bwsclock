﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BWSClock
{
    public partial class fmSettings : Form
    {
        private ProcessParams _params = null;
        private ProcessParams _org_params = null;

        public event EventHandler ParamsChangeed;

        public fmSettings()
        {
            InitializeComponent();
        }

        private void fmSettings_Shown(object sender, EventArgs e)
        {
            if (_org_params != null)
                SetEachValues();
        }

        //~fmSettings()
        //{

        //}

        private void fmSettings_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void SetEachValues()
        {
            btnBKGFrom.BackColor = _org_params._data.BackgroundLinearColorFrom;
            btnBKGTo.BackColor = _org_params._data.BackgroundLinearColorTo;
            btnSpecHourFontColor.BackColor = _org_params._data.SpecHourFontColor;
            btnHourFontColor.BackColor = _org_params._data.HourFontColor;
            btnDateFontColor.BackColor = _org_params._data.DateFontColor;
            btnTimerFontColor.BackColor = _org_params._data.TimerFontColor;
            btnWeekFontColor.BackColor = _org_params._data.WeekFontColor;
            btnHourHand.BackColor = _org_params._data.HourHandColor;
            btnMinuteHand.BackColor = _org_params._data.MinuteHandColor;
            btnSecondHand.BackColor = _org_params._data.SecondHandColor;
            btnHourFont.Tag = _org_params._data.HourFont;
            btnHourFont.Text = _org_params._data.HourFont.Name;
            btnDateFont.Tag = _org_params._data.DateFont;
            btnDateFont.Text = _org_params._data.DateFont.Name;
            btnWeekFont.Tag = _org_params._data.WeekFont;
            btnWeekFont.Text = _org_params._data.WeekFont.Name;
            btnTimerFont.Tag = _org_params._data.TimerFont;
            btnTimerFont.Text = _org_params._data.TimerFont.Name;
            CheckAlarms();
        }

        private void CheckAlarms()
        {
            lstvAlarms.Items.Clear();
            foreach (AlarmData ad in _org_params._data.Alarms)
            {
                if (ad != null)
                {
                    lstvAlarms.Items.Add(AlarmDataToListViewItem(ad));
                    ListViewItem lvi = lstvAlarms.Items[lstvAlarms.Items.Count - 1];
                    lvi.Text = lstvAlarms.Items.Count.ToString();
                }
            }
        }

        private void ChangeSettings()
        {
            _params._data.BackgroundLinearColorFrom = btnBKGFrom.BackColor;
            _params._data.BackgroundLinearColorTo = btnBKGTo.BackColor;
            _params._data.SpecHourFontColor = btnSpecHourFontColor.BackColor;
            _params._data.HourFontColor = btnHourFontColor.BackColor;
            _params._data.DateFontColor = btnDateFontColor.BackColor;
            _params._data.TimerFontColor = btnTimerFontColor.BackColor;
            _params._data.WeekFontColor = btnWeekFontColor.BackColor;
            _params._data.HourHandColor = btnHourHand.BackColor;
            _params._data.MinuteHandColor = btnMinuteHand.BackColor;
            _params._data.SecondHandColor = btnSecondHand.BackColor;
            _params._data.HourFont = (Font)btnHourFont.Tag;
            _params._data.DateFont = (Font)btnDateFont.Tag;
            _params._data.WeekFont = (Font)btnWeekFont.Tag;
            _params._data.TimerFont = (Font)btnTimerFont.Tag;
        }

        private void ButtonColorChange(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            colorDialog1.Color = btn.BackColor;
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                btn.BackColor = colorDialog1.Color;
                SettingsUpdate();
            }
        }

        private void ButtonFontChange(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            if (btn.Tag != null)
                fontDialog1.Font = (Font)((Font)(btn.Tag)).Clone();
            if (fontDialog1.ShowDialog() == DialogResult.OK)
            {
                btn.Tag = (Font)fontDialog1.Font.Clone();
                btn.Text = fontDialog1.Font.Name;
                SettingsUpdate();
            }

        }

        private void SettingsUpdate()
        {
            ChangeSettings();
            ParamsChangeed?.Invoke(this, null);
        }

        public void SetParams(ProcessParams pparams)
        {
            if (_params == null)
                _params = new ProcessParams();
            if (_org_params == null)
                _org_params = new ProcessParams();
            _params.CopyFrom(pparams);
            _org_params.CopyFrom(pparams);
        }

        public ProcessParams GetParams()
        {
            return _params;
        }

        public ProcessParams RestoreParams()
        {
            return _org_params;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void fmSettings_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
            {
                DialogResult = DialogResult.Cancel;
            }
        }

        private void btnAlarmAction_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            int alarm_type = Convert.ToInt16(btn.Tag);
            fmSetAlarm fsa = new fmSetAlarm();
            fsa.SetAlarmType(alarm_type);
            int alarm_idx = -1;
            if (lstvAlarms.SelectedIndices.Count == 1)
            {
                alarm_idx = lstvAlarms.SelectedIndices[0];
                fsa.SetAlarmData(_params.GetAlarmDataByIndex(alarm_idx));
            }
            else
            {
                if ((fmSetAlarm.AlarmActions)alarm_type == fmSetAlarm.AlarmActions.Add)
                    fsa.SetDefaultAlarmData();
                else
                    return;
            }

            if (fsa.ShowDialog(this) == DialogResult.OK)
            {
                AlarmData ad = fsa.GetAlarmData();
                ListViewItem lvi = null;
                switch ((fmSetAlarm.AlarmActions)alarm_type)
                {
                    case fmSetAlarm.AlarmActions.Edit:
                        // edit
                        _params.EditAlarm(alarm_idx, ad);
                        lvi = lstvAlarms.Items[alarm_idx];
                        SetAlarmDataToListViewItem(ad, lvi);
                        break;
                    case fmSetAlarm.AlarmActions.Delete:
                        // delete
                        _params.RemoveAlarm(alarm_idx);
                        lstvAlarms.Items.RemoveAt(alarm_idx);
                        break;
                    default:
                        // add
                        _params.AddAlarm(ad);
                        lstvAlarms.Items.Add(AlarmDataToListViewItem(ad));
                        lvi = lstvAlarms.Items[lstvAlarms.Items.Count - 1];
                        lvi.Text = lstvAlarms.Items.Count.ToString();
                        break;
                }
                
            }
        }

        private void SetAlarmDataToListViewItem(AlarmData ad, ListViewItem lvi)
        {
            if (lvi.SubItems.Count == 1)
            {
                if (ad.Type == AlarmType.EveryDay)
                    lvi.SubItems.Add(ad.Time.ToString("HH:mm:ss"));
                else
                    lvi.SubItems.Add(ad.Time.ToString("yyyy/MM/dd HH:mm:ss"));
                lvi.SubItems.Add(ad.Comment);
            }
            else
            {
                if (ad.Type == AlarmType.EveryDay)
                    lvi.SubItems[1].Text = ad.Time.ToString("HH:mm:ss");
                else
                    lvi.SubItems[1].Text = ad.Time.ToString("yyyy/MM/dd HH:mm:ss");
                lvi.SubItems[2].Text = ad.Comment;
            }
        }

        private ListViewItem AlarmDataToListViewItem(AlarmData ad)
        {
            ListViewItem ret = new ListViewItem();

            SetAlarmDataToListViewItem(ad, ret);

            return ret;
        }

        private void lstvAlarms_DoubleClick(object sender, EventArgs e)
        {
            int alarm_idx = -1;
            if (lstvAlarms.SelectedIndices.Count == 1)
                alarm_idx = lstvAlarms.SelectedIndices[0];
            else
                return;
            fmSetAlarm fsa = new fmSetAlarm();
            fsa.SetAlarmType((int)fmSetAlarm.AlarmActions.Edit);
            fsa.SetAlarmData(_params.GetAlarmDataByIndex(alarm_idx));
            if (fsa.ShowDialog(this) == DialogResult.OK)
            {
                AlarmData ad = fsa.GetAlarmData();
                _params.EditAlarm(alarm_idx, ad);
                ListViewItem lvi = lstvAlarms.Items[alarm_idx];
                SetAlarmDataToListViewItem(ad, lvi);
            }
        }
    }
}
