﻿namespace BWSClock
{
    partial class fmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnHourFontColor = new System.Windows.Forms.Button();
            this.btnTimerFontColor = new System.Windows.Forms.Button();
            this.btnWeekFontColor = new System.Windows.Forms.Button();
            this.btnDateFontColor = new System.Windows.Forms.Button();
            this.btnSpecHourFontColor = new System.Windows.Forms.Button();
            this.btnSecondHand = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.btnMinuteHand = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.btnHourHand = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btnTimerFont = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.btnWeekFont = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnDateFont = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnHourFont = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnBKGTo = new System.Windows.Forms.Button();
            this.btnBKGFrom = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lstvAlarms = new System.Windows.Forms.ListView();
            this.chCount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chDateTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chComment = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chSound = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // colorDialog1
            // 
            this.colorDialog1.AnyColor = true;
            this.colorDialog1.FullOpen = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnHourFontColor);
            this.groupBox1.Controls.Add(this.btnTimerFontColor);
            this.groupBox1.Controls.Add(this.btnWeekFontColor);
            this.groupBox1.Controls.Add(this.btnDateFontColor);
            this.groupBox1.Controls.Add(this.btnSpecHourFontColor);
            this.groupBox1.Controls.Add(this.btnSecondHand);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.btnMinuteHand);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.btnHourHand);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.btnTimerFont);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.btnWeekFont);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnDateFont);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnHourFont);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnBKGTo);
            this.groupBox1.Controls.Add(this.btnBKGFrom);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(399, 156);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Color";
            // 
            // btnHourFontColor
            // 
            this.btnHourFontColor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnHourFontColor.Location = new System.Drawing.Point(198, 40);
            this.btnHourFontColor.Name = "btnHourFontColor";
            this.btnHourFontColor.Size = new System.Drawing.Size(36, 23);
            this.btnHourFontColor.TabIndex = 4;
            this.btnHourFontColor.UseVisualStyleBackColor = true;
            this.btnHourFontColor.Click += new System.EventHandler(this.ButtonColorChange);
            // 
            // btnTimerFontColor
            // 
            this.btnTimerFontColor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTimerFontColor.Location = new System.Drawing.Point(159, 121);
            this.btnTimerFontColor.Name = "btnTimerFontColor";
            this.btnTimerFontColor.Size = new System.Drawing.Size(75, 23);
            this.btnTimerFontColor.TabIndex = 10;
            this.btnTimerFontColor.UseVisualStyleBackColor = true;
            this.btnTimerFontColor.Click += new System.EventHandler(this.ButtonColorChange);
            // 
            // btnWeekFontColor
            // 
            this.btnWeekFontColor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnWeekFontColor.Location = new System.Drawing.Point(159, 94);
            this.btnWeekFontColor.Name = "btnWeekFontColor";
            this.btnWeekFontColor.Size = new System.Drawing.Size(75, 23);
            this.btnWeekFontColor.TabIndex = 8;
            this.btnWeekFontColor.UseVisualStyleBackColor = true;
            this.btnWeekFontColor.Click += new System.EventHandler(this.ButtonColorChange);
            // 
            // btnDateFontColor
            // 
            this.btnDateFontColor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDateFontColor.Location = new System.Drawing.Point(159, 67);
            this.btnDateFontColor.Name = "btnDateFontColor";
            this.btnDateFontColor.Size = new System.Drawing.Size(75, 23);
            this.btnDateFontColor.TabIndex = 6;
            this.btnDateFontColor.UseVisualStyleBackColor = true;
            this.btnDateFontColor.Click += new System.EventHandler(this.ButtonColorChange);
            // 
            // btnSpecHourFontColor
            // 
            this.btnSpecHourFontColor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSpecHourFontColor.Location = new System.Drawing.Point(159, 40);
            this.btnSpecHourFontColor.Name = "btnSpecHourFontColor";
            this.btnSpecHourFontColor.Size = new System.Drawing.Size(36, 23);
            this.btnSpecHourFontColor.TabIndex = 3;
            this.btnSpecHourFontColor.UseVisualStyleBackColor = true;
            this.btnSpecHourFontColor.Click += new System.EventHandler(this.ButtonColorChange);
            // 
            // btnSecondHand
            // 
            this.btnSecondHand.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSecondHand.Location = new System.Drawing.Point(314, 67);
            this.btnSecondHand.Name = "btnSecondHand";
            this.btnSecondHand.Size = new System.Drawing.Size(75, 23);
            this.btnSecondHand.TabIndex = 13;
            this.btnSecondHand.UseVisualStyleBackColor = true;
            this.btnSecondHand.Click += new System.EventHandler(this.ButtonColorChange);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(239, 72);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 12);
            this.label8.TabIndex = 15;
            this.label8.Text = "Second hand:";
            // 
            // btnMinuteHand
            // 
            this.btnMinuteHand.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMinuteHand.Location = new System.Drawing.Point(314, 40);
            this.btnMinuteHand.Name = "btnMinuteHand";
            this.btnMinuteHand.Size = new System.Drawing.Size(75, 23);
            this.btnMinuteHand.TabIndex = 12;
            this.btnMinuteHand.UseVisualStyleBackColor = true;
            this.btnMinuteHand.Click += new System.EventHandler(this.ButtonColorChange);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(240, 45);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 12);
            this.label7.TabIndex = 13;
            this.label7.Text = "Minute hand:";
            // 
            // btnHourHand
            // 
            this.btnHourHand.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnHourHand.Location = new System.Drawing.Point(314, 13);
            this.btnHourHand.Name = "btnHourHand";
            this.btnHourHand.Size = new System.Drawing.Size(75, 23);
            this.btnHourHand.TabIndex = 11;
            this.btnHourHand.UseVisualStyleBackColor = true;
            this.btnHourHand.Click += new System.EventHandler(this.ButtonColorChange);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(249, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 12);
            this.label6.TabIndex = 11;
            this.label6.Text = "Hour hand:";
            // 
            // btnTimerFont
            // 
            this.btnTimerFont.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTimerFont.Location = new System.Drawing.Point(78, 121);
            this.btnTimerFont.Name = "btnTimerFont";
            this.btnTimerFont.Size = new System.Drawing.Size(75, 23);
            this.btnTimerFont.TabIndex = 9;
            this.btnTimerFont.Text = "button6";
            this.btnTimerFont.UseVisualStyleBackColor = true;
            this.btnTimerFont.Click += new System.EventHandler(this.ButtonFontChange);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "Timer Font:";
            // 
            // btnWeekFont
            // 
            this.btnWeekFont.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnWeekFont.Location = new System.Drawing.Point(78, 94);
            this.btnWeekFont.Name = "btnWeekFont";
            this.btnWeekFont.Size = new System.Drawing.Size(75, 23);
            this.btnWeekFont.TabIndex = 7;
            this.btnWeekFont.Text = "button5";
            this.btnWeekFont.UseVisualStyleBackColor = true;
            this.btnWeekFont.Click += new System.EventHandler(this.ButtonFontChange);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "Week Font:";
            // 
            // btnDateFont
            // 
            this.btnDateFont.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDateFont.Location = new System.Drawing.Point(78, 67);
            this.btnDateFont.Name = "btnDateFont";
            this.btnDateFont.Size = new System.Drawing.Size(75, 23);
            this.btnDateFont.TabIndex = 5;
            this.btnDateFont.Text = "button4";
            this.btnDateFont.UseVisualStyleBackColor = true;
            this.btnDateFont.Click += new System.EventHandler(this.ButtonFontChange);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "Date Font:";
            // 
            // btnHourFont
            // 
            this.btnHourFont.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnHourFont.Location = new System.Drawing.Point(78, 40);
            this.btnHourFont.Name = "btnHourFont";
            this.btnHourFont.Size = new System.Drawing.Size(75, 23);
            this.btnHourFont.TabIndex = 2;
            this.btnHourFont.Text = "button3";
            this.btnHourFont.UseVisualStyleBackColor = true;
            this.btnHourFont.Click += new System.EventHandler(this.ButtonFontChange);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "Hour Font:";
            // 
            // btnBKGTo
            // 
            this.btnBKGTo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBKGTo.Location = new System.Drawing.Point(159, 13);
            this.btnBKGTo.Name = "btnBKGTo";
            this.btnBKGTo.Size = new System.Drawing.Size(75, 23);
            this.btnBKGTo.TabIndex = 1;
            this.btnBKGTo.Text = "To";
            this.btnBKGTo.UseVisualStyleBackColor = true;
            this.btnBKGTo.Click += new System.EventHandler(this.ButtonColorChange);
            // 
            // btnBKGFrom
            // 
            this.btnBKGFrom.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBKGFrom.Location = new System.Drawing.Point(78, 13);
            this.btnBKGFrom.Name = "btnBKGFrom";
            this.btnBKGFrom.Size = new System.Drawing.Size(75, 23);
            this.btnBKGFrom.TabIndex = 0;
            this.btnBKGFrom.Text = "From";
            this.btnBKGFrom.UseVisualStyleBackColor = true;
            this.btnBKGFrom.Click += new System.EventHandler(this.ButtonColorChange);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "Background:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnDelete);
            this.groupBox2.Controls.Add(this.lstvAlarms);
            this.groupBox2.Controls.Add(this.btnEdit);
            this.groupBox2.Controls.Add(this.btnAdd);
            this.groupBox2.Location = new System.Drawing.Point(12, 174);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(398, 148);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Alarm (Max:20)";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(314, 116);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Tag = "2";
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnAlarmAction_Click);
            // 
            // lstvAlarms
            // 
            this.lstvAlarms.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chCount,
            this.chDateTime,
            this.chComment,
            this.chSound});
            this.lstvAlarms.FullRowSelect = true;
            this.lstvAlarms.HideSelection = false;
            this.lstvAlarms.Location = new System.Drawing.Point(6, 21);
            this.lstvAlarms.MultiSelect = false;
            this.lstvAlarms.Name = "lstvAlarms";
            this.lstvAlarms.Size = new System.Drawing.Size(301, 118);
            this.lstvAlarms.TabIndex = 0;
            this.lstvAlarms.UseCompatibleStateImageBehavior = false;
            this.lstvAlarms.View = System.Windows.Forms.View.Details;
            this.lstvAlarms.DoubleClick += new System.EventHandler(this.lstvAlarms_DoubleClick);
            // 
            // chCount
            // 
            this.chCount.Text = "ID";
            this.chCount.Width = 40;
            // 
            // chDateTime
            // 
            this.chDateTime.Text = "DateTime";
            this.chDateTime.Width = 80;
            // 
            // chComment
            // 
            this.chComment.Text = "Comment";
            this.chComment.Width = 80;
            // 
            // chSound
            // 
            this.chSound.Text = "Sound";
            this.chSound.Width = 100;
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(314, 87);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 23);
            this.btnEdit.TabIndex = 2;
            this.btnEdit.Tag = "1";
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnAlarmAction_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(314, 58);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 1;
            this.btnAdd.Tag = "0";
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAlarmAction_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(171, 328);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // fmSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 359);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.KeyPreview = true;
            this.Name = "fmSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "B.W.S. Clock Settings";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.fmSettings_FormClosed);
            this.Shown += new System.EventHandler(this.fmSettings_Shown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.fmSettings_KeyPress);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSecondHand;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnMinuteHand;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnHourHand;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnTimerFont;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnWeekFont;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnDateFont;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnHourFont;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnBKGTo;
        private System.Windows.Forms.Button btnBKGFrom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.ListView lstvAlarms;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnTimerFontColor;
        private System.Windows.Forms.Button btnWeekFontColor;
        private System.Windows.Forms.Button btnDateFontColor;
        private System.Windows.Forms.Button btnSpecHourFontColor;
        private System.Windows.Forms.Button btnHourFontColor;
        private System.Windows.Forms.ColumnHeader chCount;
        private System.Windows.Forms.ColumnHeader chDateTime;
        private System.Windows.Forms.ColumnHeader chComment;
        private System.Windows.Forms.ColumnHeader chSound;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}