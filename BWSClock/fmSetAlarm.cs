﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BWSClock
{
    public partial class fmSetAlarm : Form
    {
        public enum AlarmActions
        {
            Add = 0,
            Edit,
            Delete
        }
        private AlarmActions alarm_type = AlarmActions.Add;

        public fmSetAlarm()
        {
            InitializeComponent();
        }

        private void txtSoundFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = Application.StartupPath;
            ofd.Filter = "Audio files|*.wav;*.mp3";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                txtSoundFile.Text = ofd.FileName;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            string str_worring = string.Empty;
            switch (alarm_type)
            {
                case AlarmActions.Edit:
                case AlarmActions.Add:
                    if (txtSoundFile.Text.Length <= 0)
                        str_worring += "Sound file ";
                    if (txtComment.Text.Length <= 0)
                        str_worring += "Comment ";
                    if (str_worring.Length > 0)
                    {
                        MessageBox.Show(str_worring + "empty!", "B.W.S. Worring!!");
                        return;
                    }
                    break;
                case AlarmActions.Delete:
                    if (MessageBox.Show("Are you sure you want to delete?", "B.W.S. Worring!!") != DialogResult.OK)
                        return;
                    break;
                default:
                    break;
            }


            DialogResult = DialogResult.OK;
        }

        private void btnCancle_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public void SetAlarmType(int type)
        {
            alarm_type = (AlarmActions)type;
            switch (alarm_type)
            {
                case AlarmActions.Edit:
                    Text = "B.W.S. Edit Alarm";
                    break;
                case AlarmActions.Delete:
                    Text = "B.W.S. Delete Alarm";
                    break;
                default:
                    Text = "B.W.S. Add Alarm";
                    break;
            }
        }

        public AlarmData GetAlarmData()
        {
            AlarmData ret = new AlarmData();
            if (rbtnOnece.Checked)
                ret.Type = AlarmType.RunOnece;
            else
                ret.Type = AlarmType.EveryDay;

            string str_datetime = dtpDate.Value.ToString("yyyy/MM/dd ") + dtpTimer.Value.ToString("HH:mm:ss");
            ret.Time = Convert.ToDateTime(str_datetime);
            ret.Comment = txtComment.Text;
            ret.SoundFile = txtSoundFile.Text;
            ret.Enabled = chkbEnabled.Checked;
            return ret;
        }

        public void SetAlarmData(AlarmData ad)
        {
            if (ad.Type == (int)AlarmType.RunOnece)
                rbtnOnece.Checked = true;
            else
                rbtnEveryDay.Checked = true;
            dtpDate.Value = ad.Time;
            txtComment.Text = ad.Comment;
            txtSoundFile.Text = ad.SoundFile;
            chkbEnabled.Checked = ad.Enabled;
            string str_date = ad.Time.ToString("yyyy/MM/dd ");
            string str_time = ad.Time.ToString("HH:mm:ss");
            dtpDate.Value = Convert.ToDateTime(str_date);
            dtpTimer.Value = Convert.ToDateTime(str_time);
        }

        public void SetDefaultAlarmData()
        {
            rbtnOnece.Checked = true;
            DateTime dt_now = DateTime.Now;
            dt_now = dt_now.AddMinutes(1);
            string str_datetime = dt_now.ToString("yyyy/MM/dd HH:mm:ss");
            dtpDate.Value = Convert.ToDateTime(str_datetime);
            dtpTimer.Value = Convert.ToDateTime(str_datetime);
            txtComment.Text = string.Empty;
            txtSoundFile.Text = string.Empty;
            chkbEnabled.Checked = true;
        }

        private void fmSetAlarm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
                DialogResult = DialogResult.Cancel;
        }
    }
}
