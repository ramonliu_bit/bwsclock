﻿using NAudio.Wave;
using System.Windows.Forms;
using System.ComponentModel;

namespace BWSClock
{
    public class SoundPlayer
    {
        private BackgroundWorker _bkg = null;

        public class BKGData
        {
            public string filename;
            public bool loop;
        }

        public SoundPlayer(string file, bool loop)
        {
            _bkg = new BackgroundWorker();
            _bkg.WorkerSupportsCancellation = true;
            _bkg.DoWork += new DoWorkEventHandler(DoPlaySound);
            BKGData bd = new BKGData();
            bd.filename = file;
            bd.loop = loop;
            _bkg.RunWorkerAsync(bd);
        }

        private void DoPlaySound(object sender, DoWorkEventArgs e)
        {
            BKGData bd = e.Argument as BKGData;
            string ext = System.IO.Path.GetExtension(bd.filename).ToLower();
            if (ext.CompareTo(".wav") == 0)
                PlayWavFile(sender, bd.filename, bd.loop);
            if (ext.CompareTo(".mp3") == 0)
                PlayMp3File(sender, bd.filename, bd.loop);
        }

        public void Stop()
        {
            if (_bkg != null)
            {
                _bkg.CancelAsync();
                _bkg.Dispose();
            }
            _bkg = null;
        }

        private void PlayWavFile(object sender, string file, bool loop)
        {
            BackgroundWorker bw = sender as BackgroundWorker;
            while (!bw.CancellationPending)
            {

                using (var wfr = new WaveFileReader(file))
                using (WaveChannel32 wc = new WaveChannel32(wfr) { PadWithZeroes = false })
                using (var audioOutput = new DirectSoundOut())
                {
                    audioOutput.Init(wc);
                    audioOutput.Play();
                    while (audioOutput.PlaybackState != PlaybackState.Stopped)
                    {
                        if (bw.CancellationPending) break;
                        System.Threading.Thread.Sleep(20);
                        Application.DoEvents();
                    }
                    audioOutput.Stop();
                }
                if (!loop) break;
            }
        }

        private void PlayMp3File(object sender, string file, bool loop)
        {
            BackgroundWorker bw = sender as BackgroundWorker;
            while (!bw.CancellationPending)
            {
                using (IWavePlayer waveOutDevice = new WaveOut())
                using (AudioFileReader audioFileReader = new AudioFileReader(file))
                {
                    waveOutDevice.Init(audioFileReader);
                    waveOutDevice.Play();
                    while (waveOutDevice.PlaybackState != PlaybackState.Stopped)
                    {
                        if (bw.CancellationPending) break;
                        System.Threading.Thread.Sleep(20);
                        Application.DoEvents();
                    }
                    waveOutDevice.Stop();
                }
                if (!loop) break;
            }
        }

    }
}
