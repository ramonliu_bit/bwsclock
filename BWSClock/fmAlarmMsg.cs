﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BWSClock
{
    public partial class fmAlarmMsg : Form
    {
        public fmAlarmMsg()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Visible = !label1.Visible;
        }

        public void SetComment(string text)
        {
            label1.Text = text;
        }
    }
}
