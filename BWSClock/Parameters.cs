﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace BWSClock
{
    public enum AlarmType
    {
        RunOnece = 0,
        EveryDay,
    }

    [Serializable]
    public class AlarmData
    {
        public AlarmType Type { get; set; }
        public DateTime Time { get; set; }
        public string Comment { get; set; }
        public string SoundFile { get; set; }
        public bool Enabled { get; set; }
    }

    [Serializable]
    public class Params
    {
        public Color BackgroundLinearColorFrom { get; set; }
        public Color BackgroundLinearColorTo { get; set; }
        public Color HourHandColor { get; set; }
        public Color MinuteHandColor { get; set; }
        public Color SecondHandColor { get; set; }

        public Color HourFontColor { get; set; }
        public Color SpecHourFontColor { get; set; }
        public Color DateFontColor { get; set; }
        public Color WeekFontColor { get; set; }
        public Color TimerFontColor { get; set; }

        public Font HourFont { get; set; }
        public Font SpecHourFont { get; set; }
        public Font DateFont { get; set; }
        public Font WeekFont { get; set; }
        public Font TimerFont { get; set; }

        public Point AppPosition { get; set; }
        public double AppOpacity { get; set; }
        public AlarmData[] Alarms { get; set; }
    }

    public class ProcessParams
    {
        public Params _data = null;
        public AlarmData[] _alarms = null;

        public ProcessParams()
        {
            _data = new Params();
            _data.BackgroundLinearColorFrom = Color.FromArgb(0, 0, 100);
            _data.BackgroundLinearColorTo = Color.FromArgb(0, 0, 255);
            _data.HourHandColor = Color.White;
            _data.MinuteHandColor = Color.White;
            _data.SecondHandColor = Color.Red;
            _data.HourFont = new Font("Arial", 20);
            _data.SpecHourFont = new Font("Arial", 20);
            _data.DateFont = new Font("Arial", 20);
            _data.WeekFont = new Font("Arial", 20);
            _data.TimerFont = new Font("Arial", 20);
            _data.HourFontColor = Color.Silver;
            _data.SpecHourFontColor = Color.White;
            _data.DateFontColor = Color.LightSkyBlue;
            _data.WeekFontColor = Color.LightSkyBlue;
            _data.TimerFontColor = Color.LightCoral;
            _data.AppPosition = new Point(0, 0);
            _data.Alarms = new AlarmData[0];
            _alarms = new AlarmData[0];
            _data.AppOpacity = 1.0;
            Array.Clear(_data.Alarms, 0, _data.Alarms.Length);
        }

        ~ProcessParams()
        {
            _data.HourFont.Dispose();
            _data.SpecHourFont.Dispose();
            _data.DateFont.Dispose();
            _data.WeekFont.Dispose();
            _data.TimerFont.Dispose();
            _data = null;
        }

        public void SaveToFile(string filename)
        {
            using (FileStream fs = new FileStream(filename, FileMode.Create))
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(fs, _data);
                fs.Flush();
            }
        }

        public void LoadFromFile(string filename)
        {
            using (FileStream fs = new FileStream(filename, FileMode.Open))
            {
                BinaryFormatter bf = new BinaryFormatter();
                _data = (Params)bf.Deserialize(fs);
                _alarms = (AlarmData[])ObjectClone(_data.Alarms);
            }
        }

        public void CopyFrom(ProcessParams pparam)
        {
            _data = ObjectClone(pparam._data) as Params;
            _alarms = ObjectClone(pparam._data.Alarms) as AlarmData[];
        }

        public Params Clones()
        {
            Params ret = null;
            try
            {
                ret = (Params)ObjectClone(_data);
            }
            catch (Exception)
            {
                throw new Exception("params clone fails");
            }
            return ret;
        }

        public Object ObjectClone(Object obj_from)
        {
            Object obj_to = null;
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, obj_from);
                ms.Position = 0;
                obj_to = bf.Deserialize(ms);
            }
            return obj_to;
        }

        public int AddAlarm(AlarmData adm)
        {
            int ret = -1;

            Array.Resize<AlarmData>(ref _alarms, _alarms.Length + 1);
            ret = _alarms.Length - 1;
            _alarms[ret] = (AlarmData)ObjectClone(adm);

            AlarmDataUpdate();
            return ret;
        }

        public void EditAlarm(int index, AlarmData adm)
        {
            if (index < 0 || index >= _alarms.Length)
                return;
            _alarms[index] = (AlarmData)ObjectClone(adm);
            AlarmDataUpdate();
        }

        public void RemoveAlarm(int index)
        {
            if (index < 0 || index >= _alarms.Length)
                return;

            if (_alarms.Length <= 0)
                return;

            for (int i = index; i < _alarms.Length; i++)
            {
                if (i + 1 < _alarms.Length)
                    _alarms[i] = _alarms[i + 1];
                else
                    break;
            }
            Array.Resize<AlarmData>(ref _alarms, _alarms.Length - 1);
            AlarmDataUpdate();
        }

        private void AlarmDataUpdate()
        {
            try
            {
                _data.Alarms = (AlarmData[])ObjectClone(_alarms);
            }
            catch (Exception)
            {
                throw new Exception("alarm data clone fail.");
            }
            
        }

        public AlarmData GetAlarmDataByIndex(int alarm_idx)
        {
            if (alarm_idx < 0 || alarm_idx >= _alarms.Length)
                return null;
            return _alarms[alarm_idx];
        }
    }
}
