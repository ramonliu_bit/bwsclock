﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using BWSClock.WinApis;
using System.IO;


namespace BWSClock
{
   
    public partial class fmMain : Form, IMessageFilter
    {
        private Bitmap _bmpBackground = null;
        private Point _mouseOffset;
        private bool _isMouseDown = false;
        //private int _cntAutoHide = 0;
        private ProcessParams _params = null;
        private bool _alarming = false;
        private SoundPlayer _sound_player = null;
        private fmAlarmMsg fm_alarm_msg = null;

        public fmMain()
        {
            InitializeComponent();
            Application.AddMessageFilter(this);
        }

        private void fmMain_Shown(object sender, EventArgs e)
        {
            // check configure file
            _params = new ProcessParams();
            string cfg = Path.ChangeExtension(Application.ExecutablePath, ".cfg");
            if (File.Exists(cfg))
            {
                _params.LoadFromFile(cfg);
                Location = _params._data.AppPosition;
                Opacity = _params._data.AppOpacity;
            }
            else
            {
                Screen main = Screen.PrimaryScreen;
                Location = new Point(main.WorkingArea.Width - Width, 0);
                _params._data.AppPosition = Location;
            }
            _bmpBackground = new Bitmap(300, 300);
            FillBackgroundToBlack(_bmpBackground);
            DrawClockOnBitmap(_bmpBackground, DateTime.Now);
            BackgroundImage = _bmpBackground;
            BackgroundImageLayout = ImageLayout.Stretch;
            //_cntAutoHide = 0;
        }

        private void fmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveParametersToFile();
            _params = null;

            if (_bmpBackground != null)
            {
                _bmpBackground.Dispose();
                _bmpBackground = null;
            }
        }

        private void SaveParametersToFile()
        {
            string cfg = Path.ChangeExtension(Application.ExecutablePath, ".cfg");
            _params.SaveToFile(cfg);
        }

        private void DrawClockOnBitmap(Bitmap bmp, DateTime dtcurr)
        {
            using (Graphics g = Graphics.FromImage(bmp))
            {
                // draw 3D circle
                int gap = 5;
                bool color_dir = false;
                Color cl_base_from = _params._data.BackgroundLinearColorFrom;
                Color cl_base_to = _params._data.BackgroundLinearColorTo;
                for (int i = 0; i < 3; i++)
                {
                    Rectangle rect = new Rectangle(gap * i, gap * i, bmp.Width - (gap * i * 2), bmp.Height - (gap * i * 2));
                    color_dir = !color_dir;
                    Color clfrom = color_dir ? cl_base_from : cl_base_to;
                    Color clto = color_dir ? cl_base_to : cl_base_from;
                    using (LinearGradientBrush brush =
                        new LinearGradientBrush(rect, clfrom, clto, LinearGradientMode.ForwardDiagonal))
                    {
                        g.FillEllipse(brush, rect);
                    }
                }

                // draw hour label
                //int font_size = 20;
                int x, y;
                //using (Font pen_font = new Font("新細明體", font_size, FontStyle.Bold)) //"Arial"
                using (Font hour_font = (Font)_params._data.HourFont.Clone())
                using (Font date_font = (Font)_params._data.DateFont.Clone())
                using (Font week_font = (Font)_params._data.WeekFont.Clone())
                using (Font timer_font = (Font)_params._data.TimerFont.Clone())
                using (SolidBrush pen_brush = new SolidBrush(_params._data.HourFontColor))
                using (SolidBrush pen2_brush = new SolidBrush(_params._data.SpecHourFontColor))
                using (SolidBrush time_brush = new SolidBrush(_params._data.TimerFontColor))
                using (SolidBrush date_brush = new SolidBrush(_params._data.DateFontColor))
                {
                    int distance = (bmp.Width / 2) - (gap * 3);
                    int[] spec_hour = { 3, 6, 9, 12 };
                    for (int i = 1; i <= 12; i++)
                    {
                        // angle
                        double label_angle = 2.0 * Math.PI * i / 12;

                        // center
                        x = (bmp.Width / 2);
                        y = (bmp.Height / 2);

                        // shift about label widht and height
                        SizeF label_size = g.MeasureString(i.ToString(), hour_font, bmp.Width);
                        x -= (int)(label_size.Width / 2);
                        y -= (int)(label_size.Height / 2);

                        x += (int)(Math.Sin(label_angle) * distance);
                        y += (int)(Math.Cos(label_angle) * -distance);

                        SolidBrush sb = pen_brush;
                        if (Array.IndexOf(spec_hour, i) >= 0)
                            sb = pen2_brush;
                        g.DrawString(i.ToString(), hour_font, sb, x, y);
                    }

                    // Date
                    string str_date = dtcurr.ToString("yyyy.MM.dd");
                    SizeF date_size = g.MeasureString(str_date, date_font, bmp.Width);
                    x = (bmp.Width / 2) - ((int)date_size.Width / 2);
                    y = (bmp.Height / 2) - (int)date_size.Height * 3;
                    g.DrawString(str_date, date_font, date_brush, x, y);

                    // weekend
                    Color cl_week = _params._data.WeekFontColor;
                    string str_week = GetDayName(dtcurr, out cl_week);
                    SizeF week_size = g.MeasureString(str_week, week_font, bmp.Width);
                    x = (bmp.Width / 2) - ((int)week_size.Width / 2);
                    y += (int)date_size.Height + 8;
                    using (SolidBrush week_brush = new SolidBrush(cl_week))
                        g.DrawString(str_week, week_font, week_brush, x, y);

                    // show current time
                    string str_time = dtcurr.ToString("HH:mm:ss");
                    SizeF time_size = g.MeasureString(str_time, timer_font, bmp.Width);
                    x = (bmp.Width / 2) - ((int)time_size.Width / 2);
                    y = (bmp.Height / 2) + (int)time_size.Height * 2;
                    g.DrawString(str_time, timer_font, time_brush, x, y);
                }

                // caculation and draw time bars
                g.TranslateTransform(bmp.Width / 2, bmp.Height / 2, MatrixOrder.Append);                           
                double timer_value = 0;
                for (int i = 0; i < 3; i++)
                {
                    int timer_extent = (bmp.Width / 2) - (int)_params._data.HourFont.Size;
                    Color color_timer_bar = _params._data.HourHandColor;
                    switch (i)
                    {
                        case 1:
                            timer_value = (dtcurr.Minute + timer_value) / 60.0;
                            color_timer_bar = _params._data.MinuteHandColor;
                            break;
                        case 2:
                            int hour12 = Convert.ToInt16(dtcurr.ToString("%h"));
                            timer_value = (hour12 + timer_value) / 12.0;
                            timer_extent -= (int)_params._data.HourFont.Size * 2;
                            color_timer_bar = _params._data.HourHandColor;
                            break;
                        default:
                            timer_value = dtcurr.Second / 60.0;
                            color_timer_bar = _params._data.SecondHandColor;
                            break;
                    }
                    double timer_angle = 2.0 * Math.PI * timer_value;
                    Point timer_bar_pt = new Point((int)(timer_extent * Math.Sin(timer_angle)), (int)(-timer_extent * Math.Cos(timer_angle)));
                    using (Pen timer_pen = new Pen(color_timer_bar, 2 * (i + 1)))
                    {
                        timer_pen.SetLineCap(LineCap.RoundAnchor, LineCap.ArrowAnchor, DashCap.Flat);
                        g.DrawLine(timer_pen, new Point(0, 0), timer_bar_pt);
                    }
                    timer_extent -= (gap * 3);
                }
            }
        }

        private string GetDayName(DateTime dt, out Color cl)
        {
            string ret = string.Empty;
            cl = _params._data.WeekFontColor;
            switch (dt.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    ret = "星期一";
                    break;
                case DayOfWeek.Tuesday:
                    ret = "星期二";
                    break;
                case DayOfWeek.Wednesday:
                    ret = "星期三";
                    break;
                case DayOfWeek.Thursday:
                    ret = "星期四";
                    break;
                case DayOfWeek.Friday:
                    ret = "星期五";
                    break;
                case 
                    DayOfWeek.Saturday:
                    ret = "星期六";
                    cl = Color.LightGreen;
                    break;
                case 
                    DayOfWeek.Sunday:
                    ret = "星期日";
                    cl = Color.LightPink;
                    break;
            }
            return ret;
        }

        private void FillBackgroundToBlack(Bitmap bmp)
        {
            using (Graphics g = Graphics.FromImage(bmp))
            using (SolidBrush sb = new SolidBrush(Color.Black))
            {
                g.FillRectangle(sb, 0, 0, bmp.Width, bmp.Height);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            DateTime dt_current = DateTime.Now;
            BackgroundImage = null;
            DrawClockOnBitmap(_bmpBackground, dt_current);
            BackgroundImage = _bmpBackground;
            //if (_cntAutoHide < 5)
            //    _cntAutoHide++;
            //if ((_cntAutoHide >= 5) && (_cntAutoHide != 100))
            //{
            //    _cntAutoHide++;
            //    if (Opacity > 0.3)
            //        Opacity -= 0.1;
            //    else
            //        _cntAutoHide = 100;
            //}
            CheckAlarms(dt_current);
            timer1.Enabled = true;
        }

        private void fmMain_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (_alarming)
                {
                    _sound_player.Stop();
                    _alarming = false;
                    fm_alarm_msg.Dispose();
                }
                else
                {
                    _mouseOffset = new Point(-e.X, -e.Y);
                    _isMouseDown = true;
                }
            } else if (e.Button == MouseButtons.Right)
            {
                fmSettings settings = new fmSettings();
                settings.ParamsChangeed += new EventHandler(SettingsParamsChanged);
                settings.SetParams(_params);
                if (settings.ShowDialog(this) == DialogResult.OK)
                {
                    _params.CopyFrom(settings.GetParams());
                    SaveParametersToFile();
                }
                else
                {
                    _params.CopyFrom(settings.RestoreParams());
                }
                settings = null;
            }
        }

        private void SettingsParamsChanged(object sender, EventArgs e)
        {
            fmSettings settings = sender as fmSettings;
            _params._data = settings.GetParams().Clones();
        }

        private void fmMain_MouseMove(object sender, MouseEventArgs e)
        {
            if (_isMouseDown)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(_mouseOffset.X, _mouseOffset.Y);
                Location = mousePos;
                _params._data.AppPosition = Location;
            }
        }

        private void fmMain_MouseUp(object sender, MouseEventArgs e)
        {
            // Changes the isMouseDown field so that the form does
            // not move unless the user is pressing the left mouse button.
            if (e.Button == MouseButtons.Left)
            {
                _isMouseDown = false;
            }
        }

        private void fmMain_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Close();
            }
        }

        public bool PreFilterMessage(ref Message m)
        {
            if (m.Msg == (int)User32.MESSAGE.WM_MOUSEWEEL)
            {
                double opacity = Opacity;
                if ((int)m.WParam > 0)
                {
                    opacity += 0.2;
                    if (opacity >= 1.0)
                    {
                        opacity = 1.0;
                        //_cntAutoHide = 0;
                    }
                }
                else
                {
                    opacity -= 0.2;
                    if (opacity <= 0.25) opacity = 0.25;
                }
                Opacity = opacity;
                _params._data.AppOpacity = opacity;
                //// WM_MOUSEWHEEL, find the control at screen position m.LParam
                //Point pos = new Point(m.LParam.ToInt32() & 0xffff, m.LParam.ToInt32() >> 16);
                //IntPtr hWnd = WindowFromPoint(pos);
                //if (hWnd != IntPtr.Zero && hWnd != m.HWnd && Control.FromHandle(hWnd) != null)
                //{
                //    SendMessage(hWnd, m.Msg, m.WParam, m.LParam);
                //    return true;
                //}
            }
            return false;
        }

        private void fmMain_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
                Close();
        }

        private void CheckAlarms(DateTime dtcurr)
        {
            if (_alarming) return;
            string comment = string.Empty;
            string soundfile = string.Empty;
            for (int i = 0; i < _params._data.Alarms.Length; i++)
            {
                AlarmData alarm = _params._data.Alarms[i];
                if ((alarm != null) && (alarm.Enabled))
                {
                    if (dtcurr < alarm.Time)
                        continue;
                    if (alarm.Type == AlarmType.RunOnece)
                        alarm.Enabled = false;
                    else
                        alarm.Time = alarm.Time.AddDays(1);
                    _params._data.Alarms[i] = alarm;
                    comment = alarm.Comment;
                    soundfile = alarm.SoundFile;
                    SaveParametersToFile();
                    break;
                }
            }
            if (comment.Length > 0)
            {   
                _sound_player = new SoundPlayer(soundfile, true);
                _alarming = true;
                ShowAlarmMessage(comment);
            }
        }

        private void ShowAlarmMessage(string comment)
        {
            fm_alarm_msg = new fmAlarmMsg();
            fm_alarm_msg.SetComment(comment);
            fm_alarm_msg.Width = Screen.PrimaryScreen.WorkingArea.Width - 20;
            fm_alarm_msg.Height = Screen.PrimaryScreen.WorkingArea.Height / 2;
            fm_alarm_msg.Location = new Point(10, Screen.PrimaryScreen.WorkingArea.Height / 2);
            fm_alarm_msg.Show();
        }
    }
}
