﻿namespace BWSClock
{
    partial class fmSetAlarm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkbEnabled = new System.Windows.Forms.CheckBox();
            this.rbtnOnece = new System.Windows.Forms.RadioButton();
            this.rbtnEveryDay = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSoundFile = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCancle = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.dtpTimer = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // chkbEnabled
            // 
            this.chkbEnabled.AutoSize = true;
            this.chkbEnabled.Checked = true;
            this.chkbEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkbEnabled.Location = new System.Drawing.Point(12, 12);
            this.chkbEnabled.Name = "chkbEnabled";
            this.chkbEnabled.Size = new System.Drawing.Size(62, 16);
            this.chkbEnabled.TabIndex = 2;
            this.chkbEnabled.Text = "Enabled";
            this.chkbEnabled.UseVisualStyleBackColor = true;
            // 
            // rbtnOnece
            // 
            this.rbtnOnece.AutoSize = true;
            this.rbtnOnece.Checked = true;
            this.rbtnOnece.Location = new System.Drawing.Point(12, 39);
            this.rbtnOnece.Name = "rbtnOnece";
            this.rbtnOnece.Size = new System.Drawing.Size(52, 16);
            this.rbtnOnece.TabIndex = 3;
            this.rbtnOnece.TabStop = true;
            this.rbtnOnece.Text = "Onece";
            this.rbtnOnece.UseVisualStyleBackColor = true;
            // 
            // rbtnEveryDay
            // 
            this.rbtnEveryDay.AutoSize = true;
            this.rbtnEveryDay.Location = new System.Drawing.Point(70, 39);
            this.rbtnEveryDay.Name = "rbtnEveryDay";
            this.rbtnEveryDay.Size = new System.Drawing.Size(73, 16);
            this.rbtnEveryDay.TabIndex = 4;
            this.rbtnEveryDay.TabStop = true;
            this.rbtnEveryDay.Text = "Every Day";
            this.rbtnEveryDay.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "Date Time:";
            // 
            // txtSoundFile
            // 
            this.txtSoundFile.Location = new System.Drawing.Point(70, 84);
            this.txtSoundFile.Name = "txtSoundFile";
            this.txtSoundFile.ReadOnly = true;
            this.txtSoundFile.Size = new System.Drawing.Size(251, 22);
            this.txtSoundFile.TabIndex = 6;
            this.txtSoundFile.Click += new System.EventHandler(this.txtSoundFile_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "Sound File:";
            // 
            // txtComment
            // 
            this.txtComment.Location = new System.Drawing.Point(70, 107);
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(251, 22);
            this.txtComment.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "Comment:";
            // 
            // btnCancle
            // 
            this.btnCancle.Location = new System.Drawing.Point(164, 36);
            this.btnCancle.Name = "btnCancle";
            this.btnCancle.Size = new System.Drawing.Size(75, 23);
            this.btnCancle.TabIndex = 0;
            this.btnCancle.Text = "Cancle";
            this.btnCancle.UseVisualStyleBackColor = true;
            this.btnCancle.Click += new System.EventHandler(this.btnCancle_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(246, 36);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "yyyy.MM.dd";
            this.dtpDate.Location = new System.Drawing.Point(70, 61);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(137, 22);
            this.dtpDate.TabIndex = 5;
            // 
            // dtpTimer
            // 
            this.dtpTimer.CustomFormat = "HH:mm:ss";
            this.dtpTimer.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTimer.Location = new System.Drawing.Point(213, 61);
            this.dtpTimer.Name = "dtpTimer";
            this.dtpTimer.ShowUpDown = true;
            this.dtpTimer.Size = new System.Drawing.Size(108, 22);
            this.dtpTimer.TabIndex = 8;
            // 
            // fmSetAlarm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 141);
            this.Controls.Add(this.dtpTimer);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancle);
            this.Controls.Add(this.txtComment);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtSoundFile);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rbtnEveryDay);
            this.Controls.Add(this.rbtnOnece);
            this.Controls.Add(this.chkbEnabled);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.KeyPreview = true;
            this.Name = "fmSetAlarm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "B.W.S. Set Alarm";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.fmSetAlarm_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkbEnabled;
        private System.Windows.Forms.RadioButton rbtnOnece;
        private System.Windows.Forms.RadioButton rbtnEveryDay;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSoundFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCancle;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.DateTimePicker dtpTimer;
    }
}